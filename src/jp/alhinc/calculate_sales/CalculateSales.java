package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String FILE_OVERDIGIT = "合計金額が10桁を超えました";
	private static final String FILE_INVALSALE_FORMAT =  "のフォーマットが不正です";
	private static final String FILE_INVALBRANCH_ID =  "の支店コードが不正です";
	private static final String FILE_INVALCOMMODITY_ID =  "の商品コードが不正です";
	private static final String FILE_NOT_SERIALNUMBER_ID =  "売上ファイル名が連番になっていません";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();

		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();

		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();

		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		//エラー処理：コマンドライン引数が渡されているか
		if(args.length !=1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales ,"^[0-9]+$" , "支店")) {
			return;
		}
		// 商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales,"^[a-z|A-Z|0-9]{8}$" , "商品")) {
			return;
		}

		//支店売上ファイル読み込み処理
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		//「8桁の数字+.rcd」の形式ファイルを抽出
		for(int i = 0; i < files.length; i++) {
			if(files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}

		//エラー処理：支店売上ファイルが連番になっているか
		Collections.sort(rcdFiles);
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));
			if((latter - former) != 1) {
				System.out.println(FILE_NOT_SERIALNUMBER_ID);
				return;
			}
		}

		//支店ごとの売上をMap branchSalesに格納
		for(int i = 0; i < rcdFiles.size(); i++) {
			BufferedReader br = null;

			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);
				String line;

				//支店売上ファイルのファイル名
				String fileName = rcdFiles.get(i).getName();

				//支店売上のデータを格納する
				List<String> saleDatas = new ArrayList<String>();

				//1行ごとにデータを取り出す
				while((line = br.readLine()) != null) {
					saleDatas.add(line);
				}

				//エラー処理: 売上ファイルの中身が3行ではなかった場合
				if(saleDatas.size() != 3) {
					System.out.println(fileName + FILE_INVALSALE_FORMAT);
					return;
				}

				//エラー処理:店売上ファイルの支店コードは支店定義ファイルに存在するか
				if(!branchNames.containsKey(saleDatas.get(0))) {
					System.out.println(fileName + FILE_INVALBRANCH_ID);
					return;
				}

				//エラー処理:店売上ファイルの商品コードは商品定義ファイルに存在するか
				if(!commodityNames.containsKey(saleDatas.get(1))) {
					System.out.println(fileName + FILE_INVALCOMMODITY_ID);
					return;
				}

				//エラー処理:売上データが数字かどうか
				if(!saleDatas.get(2).matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				//売上ファイル抽出
				long fileSale = Long.parseLong(saleDatas.get(2));
				Long branchsaleAmount = branchSales.get(saleDatas.get(0)) + fileSale;
				Long commoditysaleAmount = commoditySales.get(saleDatas.get(1)) + fileSale;

				//エラー処理：売り上げが10桁以下になっているか
				if(branchsaleAmount >= 10000000000L || commoditysaleAmount >= 10000000000L) {
					System.out.println(FILE_OVERDIGIT);
					return;
				}

				//支店売上データの更新
				branchSales.put(saleDatas.get(0), branchsaleAmount);

				//商品売上データの更新
				commoditySales.put(saleDatas.get(1), commoditysaleAmount);
			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}
	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> allNames, Map<String, Long> allSales ,String regex ,String fileTitle  )   {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);
			//エラー処理：ファイルが存在しない
			if(!file.exists()) {
				System.out.println(fileTitle + FILE_NOT_EXIST);
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;

			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				//文字列の分割（tems[0]：コード、items[1]：名前）
				String[] items = line.split(",");
				//エラー処理：フォーマット異常
				if((items.length != 2) || (!items[0].matches(regex))) {
					System.out.println(fileTitle + FILE_INVALID_FORMAT);
					return false;
				}
				allNames.put(items[0], items [1] );
				allSales.put(items[0], 0L);
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> allNames, Map<String, Long> allSales) {
		// 売上集計データの出力処理
		BufferedWriter bw = null;
		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			//支店売上データの書き出し
			for(String key : allNames.keySet()) {
				bw.write(key + "," + allNames.get(key) + "," + allSales.get(key));
				bw.newLine();
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {

			// ファイルが開いている場合ファイルを閉じる処理
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}